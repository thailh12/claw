const fs = require('fs')
const readline = require('readline')
const { google } = require('googleapis')
const puppeteer = require('puppeteer')
/* START CONFIG FOR GOOGLE SHEET API */

const SCOPES = [
  'https://www.googleapis.com/auth/spreadsheets.readonly',
  'https://www.googleapis.com/auth/spreadsheets',
]
const TOKEN_PATH = 'token.json'
const sheetId = 2045593616
const spreadsheetId = '1gbBcYcDbE08Cbl9vgLrkhwON-r0JOuXU50GyvFDbmvs'

/* END CONFIG FOR GOOGLE SHEET API */
function readURLFile(path) {
  return fs.readFileSync(path, 'utf-8')
}
async function RUN() {
  await crawlll()
  //   await runImportSheet()
}
setInterval(RUN, 3600 * 24 * 1000)
function runImportSheet() {
  fs.readFile('credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err)
    authorize(JSON.parse(content), listMajors)
  })
}
RUN()
function authorize(credentials, callback) {
  const { client_secret, client_id, redirect_uris } = credentials.installed
  const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0])
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback)
    oAuth2Client.setCredentials(JSON.parse(token))
    callback(oAuth2Client)
  })
}

function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  })
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  })
  rl.question('Enter the code from that page here: ', code => {
    rl.close()
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error while trying to retrieve access token', err)
      oAuth2Client.setCredentials(token)
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), err => {
        if (err) return console.error(err)
        console.log('Token stored to', TOKEN_PATH)
      })
      callback(oAuth2Client)
    })
  })
}

async function listMajors(auth) {
  const sheetsAPI = google.sheets({ version: 'v4', auth })
  const theData = await readCSVContent()
  await populateAndStyle(sheetsAPI, theData)
}
function readCSVContent() {
  return new Promise((resolve, reject) => {
    const data = readURLFile('./page.csv')
    resolve(data)
  })
}

function populateAndStyle(sheetsAPI, theData) {
  return new Promise((resolve, reject) => {
    const dataAndStyle = {
      spreadsheetId: spreadsheetId,
      resource: {
        requests: [
          {
            pasteData: {
              coordinate: {
                sheetId: sheetId,
                rowIndex: 0,
                columnIndex: 0,
              },
              data: theData,
              delimiter: ',',
            },
          },
          {
            repeatCell: {
              range: {
                sheetId: sheetId,
                startRowIndex: 0,
                endRowIndex: 1,
              },
              cell: {
                userEnteredFormat: {
                  textFormat: {
                    fontSize: 11,
                    bold: true,
                  },
                },
              },
              fields: 'userEnteredFormat(textFormat)',
            },
          },
        ],
      },
    }

    sheetsAPI.spreadsheets.batchUpdate(dataAndStyle, function(err, response) {
      if (err) {
        reject('The Sheets API returned an error: ' + err)
      } else {
        console.log(
          sheetId + ' sheet populated with ' + theData.length + ' rows and column style set.',
        )
        resolve()
      }
    })
  })
}
async function crawlll() {
  const browser = await puppeteer.launch({
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
    // executablePath: 'node_modules/puppeteer/.local-chromium_bk/linux-674921/chrome-linux',
  })

  var result2 = 'Name,Time,Rating,Content\n'
  let i = 1
  for (;;) {
    const url = `https://apps.shopify.com/pagefly/reviews?page=${i}`
    i++
    console.log(`Visiting url: ${url}`)
    let page = await browser.newPage()
    try {
      await page.goto(url)
      let dataCrawl = await page.evaluate(async () => {
        let collectionDOM = document.getElementsByClassName('review-listing')
        let result = ''
        for (let DOM of collectionDOM) {
          const domButtonMore = DOM.getElementsByClassName(
            'marketing-button--visual-link truncate-content-toggle truncate-content-toggle--show',
          )[0]
          // more content
          if (domButtonMore) {
            domButtonMore.click()
          }
          const name = DOM.querySelector('.review-listing-header__text').textContent.trim()
          const rating = parseInt(
            DOM.querySelectorAll('.review-metadata__item-value')[0].textContent.trim(),
          )
          const content = DOM.querySelector('.truncate-content-copy')
            .textContent.trim()
            .replace(/"/g, '""')
          const time = DOM.querySelectorAll('.review-metadata__item-value')[1].textContent.trim()
          result += `"${name}","${time}",${rating},"${content}"\n`
        }
        return result
      })
      // console.log('dataCrawl', dataCrawl)
      result2 += dataCrawl
      if (dataCrawl.length === 0) {
        break
      }
    } catch (err) {
      console.log(`An error occured on url: ${url}`, err)
    } finally {
      await page.close()
    }
  }
  const fileName = 'page.csv'
  fs.writeFile(fileName, result2, 'utf8', (err, data) => {
    if (err) return
  })

  await browser.close()
}
